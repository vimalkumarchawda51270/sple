/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>

#include <omp.h>

#include <lcm/lcm.h>

#include <common/time_util.h>
#include <common/zarray.h>

#include <lcmtypes/image_t.h>
#include "lcmtypes/velodyne_features_t.h"

#include "utils/gl_utils.h"

#include <vx/vx.h>
#include <vx/webvx.h>

#include "utils/image_utils.h"

#include "clusterizer.h"
#include "integral_image.h"
#include "interval_image.h"
#include "line.h"
#include "plane.h"
#include "point_cloud.h"

#define POINT_SIZE 3
#define LINE_LENGTH 5.0
#define LINE_SIZE 10
#define PLANE_SIZE 3
#define NORMAL_SCALE 0.4
#define CURVATURE_THRESHOLD 0.0

#define CROPPING_VOXEL_RADIUS 0.2
#define CROPPING_MIN_POINTS 4
#define INTERVAL_RANGE_THRESHOLD 300
#define INTERVAL_CIRCULAR_LEVELS 2
#define INTERVAL_ROW_START 1
#define INTERVAL_COL_START 1
#define NORMAL_MIN_POINTS 5
#define CLUSTER_POINT_MIN_RANGE 2.5
#define CLUSTER_POINT_DISTANCE_THRESHOLD 1.5
#define CLUSTER_NORMAL_ANGLE_THRESHOLD 0.5 // 18 degrees
#define CLUSTER_MIN_DIMENSION 10 
#define CLUSTER_NEIGHBOURING_RADIUS 5
#define LINE_MAX_ERROR 0.1
#define LINE_MAX_EIGENVALUES_RATIO 0.01
#define LINE_MIN_LENGTH 1.0
#define PLANE_MAX_ERROR 0.05
#define PLANE_MAX_EIGENVALUES_RATIO 0.001
#define PLANE_MIN_POINTS 150

struct state {
  // lcm fields
  lcm_t *lcm;

  // viewer fields
  int enable_viewer;
  int point_size;
  int line_size;
  int plane_size;
  double line_length;
  double normal_scale;
  double curvature_threshold;
  vx_world_t *vw;
  webvx_t *webvx;

  // velodyne fields
  double *vertical_angle_sincos;  

  // data structures 
  uint16_t interval_range_threshold;
  int cropping_min_points;
  int interval_circular_levels;
  int interval_row_start;
  int interval_col_start;
  int normal_min_points;
  int cluster_min_dimension;
  int cluster_neighbouring_radius;
  int plane_min_points;
  double cropping_voxel_radius;
  double cluster_point_min_range;
  double cluster_point_distance_threshold;
  double cluster_normal_angle_threshold;
  double line_max_error;
  double line_max_eigenvalues_ratio;
  double line_min_length;
  double plane_max_error;
  double plane_max_eigenvalues_ratio;
  point_cloud_t *full_point_cloud;
  point_cloud_t *point_cloud;
  integral_image_t *point_integral_image;
};
typedef struct state state_t;

double vertical_angle_degrees[32] = {
   10.67,   9.33,   8.00,   6.67,
    5.33,   4.00,   2.67,   1.33, 
    0.00,  -1.33,  -2.67,  -4.00,
   -5.33,  -6.66,  -8.00,  -9.33,
  -10.67, -12.00, -13.33, -14.67,
  -16.00, -17.33, -18.67, -20.00,
  -21.33, -22.67, -24.00, -25.33,
  -26.66, -28.00, -29.33, -30.67  
};

int jbud_event_handler(vx_layer_t *vl, const vx_event_t *event, void *user) {
  state_t *state = (state_t*)user;

  int key_code = -1;
  switch(event->type) {
  case VX_EVENT_KEY_DOWN:
    key_code = event->u.key.key_code;
    break;
  default: {}
  }

  switch(key_code) {
  case 82: // r    
    break;
  case 49: // 1
    break;
  case 48: // 0
    break;
  default: {}
  }

  return 0;
}

void on_create_canvas(vx_canvas_t *vc, const char *name, void *impl) {
  state_t *state = (state_t*)impl;

  printf("[INFO] on create canvas\n");
  vx_layer_t *vl = vx_canvas_get_layer(vc, "default");

  vx_layer_set_world(vl, state->vw);
  vx_layer_add_event_handler(vl, jbud_event_handler, 0, state);
  float background_color[4] = { 0.95, 0.95, 0.96, 1.0 };  
  vx_layer_set_background_rgba(vl, background_color, 0);
}

void on_destroy_canvas(vx_canvas_t *vc, void *impl) {
  state_t *state = (state_t*)impl;
  printf("[INFO] on destroy canvas\n");
}

void clusters_draw(zarray_t *clusters, state_t *state) {
  vx_buffer_t *vb_clusters = vx_world_get_buffer(state->vw, "clusters");
  for(int i = 0; i < zarray_size(clusters); ++i) {
    zarray_t **cluster;
    zarray_get_volatile(clusters, i, &cluster);    
    cluster_draw(*cluster, state->point_cloud, vb_clusters, state->point_size);
  }
  vx_buffer_swap(vb_clusters);  
}

void lines_draw(zarray_t *lines, state_t *state) {
  vx_buffer_t *vb_lines = vx_world_get_buffer(state->vw, "lines");
  for(int i = 0; i < zarray_size(lines); ++i) {
    line_t **line;
    zarray_get_volatile(lines, i, &line);
    line_draw(*line, state->vw, vb_lines, state->line_length, state->line_size);
  }
  vx_buffer_swap(vb_lines);
}

void planes_draw(zarray_t *planes, state_t *state) {
  vx_buffer_t *vb_planes = vx_world_get_buffer(state->vw, "planes");
  for(int i = 0; i < zarray_size(planes); ++i) {
    plane_t **plane;
    zarray_get_volatile(planes, i, &plane);
    plane_draw(*plane, state->vw, vb_planes, state->plane_size);
  }
  vx_buffer_swap(vb_planes);
}

void remove_flat_regions(image_t *depth_image, state_t *state) {
  int index = 0;
  double theta_offset = -2.0 * M_PI / depth_image->width;
  mati_t *mask_image = mati_create(depth_image->height, depth_image->width);
  int32_t *mask_data = mask_image->data;
  uint16_t *depth_data = (uint16_t*)depth_image->data;
  for(int c = 0; c < depth_image->width; ++c) {
    double theta = c * theta_offset;
    double ctheta = cos(theta);
    double stheta = sin(theta);      
    for(int r = 0; r < depth_image->height; ++r, ++index) {      
      double range = ((double)depth_data[r * depth_image->width + c]) * 0.001;
      rich_point_t *rich_point; 
      zarray_get_volatile(state->full_point_cloud->measurements, index, &rich_point);            
      if(!(range > 0.0)) {
	rich_point_set_zeros(rich_point);
	continue;
      }      
      double cpsi = state->vertical_angle_sincos[2 * r + 1];
      double spsi = state->vertical_angle_sincos[2 * r + 0];
      double horiz = cpsi * range;      
      double point[4] = { ctheta * horiz, stheta * horiz, spsi * range, 1.0 }; 
      rich_point_set_zeros(rich_point);
      rich_point_set_point(rich_point, point);      

      if(mask_data[r * depth_image->width + c] > 0) {
	continue;
      }      
      zarray_t *similar_rows = zarray_create(sizeof(int));
      zarray_add(similar_rows, &r);
      for(int rr = r + 1; rr < depth_image->height; ++rr) {
	if(mask_data[rr * depth_image->width + c] > 0) {
	  continue;
	}
	range = ((double)depth_data[rr * depth_image->width + c]) * 0.001;
	if(!(range > 0.0)) {
	  continue;
	}
	cpsi = state->vertical_angle_sincos[2 * rr + 1];
	spsi = state->vertical_angle_sincos[2 * rr + 0];
	horiz = cpsi * range;      
	double cpoint[4] = { ctheta * horiz, stheta * horiz, spsi * range, 1.0 }; 
	double diff[4] = { cpoint[0] - point[0], cpoint[1] - point[1], cpoint[1] - point[1], 0.0 }; 
	
	double collapsed_distance = sqrt(diff[0] * diff[0] + diff[1] * diff[1]);
	if(collapsed_distance < state->cropping_voxel_radius) { 
	  zarray_add(similar_rows, &rr);
	}	
      }
      if(zarray_size(similar_rows) < state->cropping_min_points) {
	depth_data[r * depth_image->width + c] = 0.0;
      }
      else {
	for(int i = 0; i < zarray_size(similar_rows); ++i) {
	  int *similar_row;
	  zarray_get_volatile(similar_rows, i, &similar_row);
	  mask_data[(*similar_row) * depth_image->width + c] = 1;		
	}
      }
      zarray_destroy(similar_rows);
    }
  }
  mati_destroy(mask_image);
}

static void velodyne_depth_image_callback(const lcm_recv_buf_t *rbuf, const char *channel,
					  const image_t *msg, void *user) {
  state_t *state = (state_t*)user;
  int64_t allocation_time = 0, unprojection_time = 0, point_integral_image_time = 0;
  int64_t interval_image_time = 0, normals_time = 0, draw_time = 0, release_time = 0;
  int64_t clustering_time = 0, fitting_time = 0, publishing_time = 0, total_time = 0; 
  int64_t flat_region_removal_time = 0;  

  // Allocate necessary structures once and for ever
  int64_t time_start = utime_now();  
  image_t *depth_image = image_t_copy(msg);
  mati_t *indices = mati_create(depth_image->height, depth_image->width);
  if(state->point_cloud == NULL) {
    state->full_point_cloud = point_cloud_create(sizeof(rich_point_t));
    state->point_cloud = point_cloud_create(sizeof(rich_point_t));
    point_cloud_pre_allocate_structures(state->full_point_cloud, 
					depth_image->width, depth_image->height);
    point_cloud_pre_allocate_structures(state->point_cloud, 
					depth_image->width, depth_image->height);
    state->point_integral_image = integral_image_create(depth_image->width, depth_image->height);
  }  
  int64_t time_stop = utime_now();
  allocation_time = time_stop - time_start;

  // Remove points belonging to regions without a vertical structure 
  time_start = utime_now();  
  remove_flat_regions(depth_image, state);
  time_stop = utime_now();
  flat_region_removal_time = time_stop - time_start;

  // Unproject the points
  time_start = utime_now();
  double theta_offset = -2.0 * M_PI / depth_image->width;
  uint16_t *depth_data = (uint16_t*)depth_image->data;
  int32_t *index = indices->data;
  int i = 0;
  for(int c = 0; c < depth_image->width; ++c) {
    double theta = c * theta_offset;
    double ctheta = cos(theta);
    double stheta = sin(theta);      
    for(int r = 0; r < depth_image->height; ++r, ++i) {      
      double range = ((double)depth_data[r * depth_image->width + c]) * 0.001;
      rich_point_t *rich_point; 
      zarray_get_volatile(state->point_cloud->measurements, i, &rich_point);      
      if(!(range > 0.0)) {
	rich_point_set_zeros(rich_point);
	index[r * indices->ncols + c] = -1;
	continue;
      }
      
      double cpsi = state->vertical_angle_sincos[2 * r + 1];
      double spsi = state->vertical_angle_sincos[2 * r + 0];
      double horiz = cpsi * range;      

      double point[4] = { ctheta * horiz, stheta * horiz, spsi * range, 1.0 }; 
      rich_point_set_zeros(rich_point);
      rich_point_set_point(rich_point, point);      
      index[r * indices->ncols + c] = i;
    }
  }  
  time_stop = utime_now();
  unprojection_time = time_stop - time_start;

  // Compute the intervals
  time_start = utime_now();
  interval_image_t *intervals = interval_image_compute(indices, depth_image,
						       state->interval_range_threshold, 
						       state->interval_circular_levels,
						       state->interval_row_start,
						       state->interval_col_start);
  time_stop = utime_now();
  interval_image_time = time_stop - time_start;

  // Compute point integral image
  time_start = utime_now();
  integral_image_point_compute(state->point_integral_image, indices, state->point_cloud);
  time_stop = utime_now();
  point_integral_image_time = time_stop - time_start;

  // Compute the normals
  time_start = utime_now();
  point_cloud_compute_normals_using_intervals(state->point_cloud, state->point_integral_image, 
					      indices, intervals, state->normal_min_points);
  time_stop = utime_now();
  normals_time = time_stop - time_start;

  // Clustering
  time_start = utime_now();
  zarray_t *clusters = cluster_point_and_normal_compute(state->point_cloud, indices, 
							state->cluster_min_dimension, 
							state->cluster_neighbouring_radius,
							state->cluster_point_distance_threshold,
							state->cluster_point_min_range,
							state->cluster_normal_angle_threshold);
  time_stop = utime_now();  
  clustering_time = time_stop - time_start;
  printf("[INFO] found %d clusters\n", zarray_size(clusters));

  // Perform line/plane fitting
  time_start = utime_now();
  zarray_t *lines, *planes;
  cluster_fitting_lines_planes_compute(&lines, &planes,
				       clusters, state->point_cloud, 
				       state->line_max_error, state->plane_max_error, 
				       state->line_max_eigenvalues_ratio, 
				       state->plane_max_eigenvalues_ratio,
				       state->line_min_length,
				       state->plane_min_points);
  time_stop = utime_now();  
  fitting_time = time_stop - time_start;
  printf("[INFO] found %d features(%d lines, %d planes)\n", 
	 zarray_size(lines) + zarray_size(planes), zarray_size(lines), zarray_size(planes));

  // Publish features
  time_start = utime_now();
  velodyne_features_t *out_msg = (velodyne_features_t*)malloc(sizeof(velodyne_features_t)); 
  out_msg->utime = depth_image->utime;
  out_msg->lines_data_length = zarray_size(lines) * 6;
  out_msg->planes_data_length = zarray_size(planes) * 6;
  out_msg->lines = (double*)malloc(out_msg->lines_data_length * sizeof(double));
  out_msg->planes = (double*)malloc(out_msg->planes_data_length * sizeof(double));
  double *lines_data = out_msg->lines;
  for(int i = 0; i < zarray_size(lines); ++i) {
    line_t **line;
    zarray_get_volatile(lines, i, &line);    
    lines_data[0] = (*line)->origin->data[0]; 
    lines_data[1] = (*line)->origin->data[1];     
    lines_data[2] = (*line)->origin->data[2];
    lines_data[3] = (*line)->direction->data[0]; 
    lines_data[4] = (*line)->direction->data[1];     
    lines_data[5] = (*line)->direction->data[2];
    lines_data += 6;
  }
  double *planes_data = out_msg->planes;
  for(int i = 0; i < zarray_size(planes); ++i) {
    plane_t **plane;
    zarray_get_volatile(planes, i, &plane);    
    planes_data[0] = (*plane)->origin->data[0]; 
    planes_data[1] = (*plane)->origin->data[1];     
    planes_data[2] = (*plane)->origin->data[2];
    planes_data[3] = (*plane)->normal->data[0]; 
    planes_data[4] = (*plane)->normal->data[1];     
    planes_data[5] = (*plane)->normal->data[2];
    planes_data += 6;
  }
  velodyne_features_t_publish(state->lcm, "VELODYNE_FEATURES", out_msg);
  time_stop = utime_now();
  publishing_time = time_stop - time_start;

  // Draw
  time_start = utime_now();
  if(state->enable_viewer) {
    float full_cloud_color[4] = { 0.03f, 0.27f, 0.49f, 1.0f };
    point_cloud_draw(state->full_point_cloud, state->vw,
    		     "points", full_cloud_color,
  		     1, 0.0, 0.0);
    float cloud_color[4] = { 0.59f, 0.0f, 0.09f, 1.0f };
    point_cloud_draw(state->point_cloud, state->vw,
    		     "vertical_points", cloud_color,
    		     state->point_size, state->normal_scale,
    		     state->curvature_threshold);
    clusters_draw(clusters, state);
    lines_draw(lines, state);
    planes_draw(planes, state);
  }
  time_stop = utime_now();
  draw_time = time_stop - time_start;
  
  // Release allocated memory
  time_start = utime_now();
  velodyne_features_t_destroy(out_msg);
  for(int i = 0; i < zarray_size(planes); ++i) {
    plane_t **plane;
    zarray_get_volatile(planes, i, &plane);
    plane_destroy(*plane);
  }
  zarray_destroy(planes);
  for(int i = 0; i < zarray_size(lines); ++i) {
    line_t **line;
    zarray_get_volatile(lines, i, &line);
    line_destroy(*line);
  }
  zarray_destroy(lines);
  for(int i = 0; i < zarray_size(clusters); ++i) {
    zarray_t **cluster;
    zarray_get_volatile(clusters, i, &cluster);    
    zarray_destroy(*cluster);
  }
  zarray_destroy(clusters);
  mati_destroy(indices);
  image_t_destroy(depth_image);
  interval_image_destroy(intervals);
  time_stop = utime_now();
  release_time = time_stop - time_start;

  total_time = flat_region_removal_time + allocation_time + unprojection_time + 
    interval_image_time + point_integral_image_time + normals_time + 
    clustering_time + fitting_time +  publishing_time + draw_time + release_time;
  printf("[INFO] timings flat_region_removal(%.3f) +\n", 
	 flat_region_removal_time / 1.0E6);
  printf("[INFO] allocation(%.3f) + unprojection(%.3f) +\n", 
	 allocation_time / 1.0E6, unprojection_time / 1.0E6);
  printf("[INFO] intervals(%.3f) + integral(%.3f) +\n", 
	 interval_image_time / 1.0E6, point_integral_image_time / 1.0E6);
  printf("[INFO] normals(%.3f) + clustering(%.3f) + fitting(%.3f) +\n", 
	 normals_time / 1.0E6, clustering_time / 1.0E6, fitting_time / 1.0E6);
  printf("[INFO] publishing(%.3f) + draw(%.3f) + release(%.3f) =\n", 
	 publishing_time / 1.0E6, draw_time / 1.0E6, release_time / 1.0E6);
  printf("[INFO] total(%.3f)\n\n", total_time / 1.0E6);
}

void init_state(state_t *state, int enable_viewer, int port) {
  state->lcm = lcm_create(NULL);
  if(!state->lcm) {
    printf("[ERROR] impossible to initialize LCM environment... quitting!");
    exit(-1);
  }

  state->point_size = POINT_SIZE;
  state->line_size = LINE_SIZE;
  state->plane_size = PLANE_SIZE;
  state->line_length = LINE_LENGTH;
  state->normal_scale = NORMAL_SCALE;
  state->curvature_threshold = CURVATURE_THRESHOLD;
  if(enable_viewer) {
    state->enable_viewer = 1;
    state->vw = vx_world_create();
    state->webvx = webvx_create_server(port, NULL, "index.html");
    printf("[INFO] viewer enabled on port %d\n", port);
    webvx_define_canvas(state->webvx, "velodyne32_features_calculator_viewer_canvas",
			on_create_canvas, on_destroy_canvas, state);

    vx_buffer_t *vb_cartesian_axes = vx_world_get_buffer(state->vw, "cartesian_axes");
    draw_cartesian_axes(state->vw, vb_cartesian_axes, 1.0, 5.0);
    vx_buffer_swap(vb_cartesian_axes);
    vx_buffer_t *vb_car_model = vx_world_get_buffer(state->vw, "car_model");
    draw_car_model(state->vw, vb_car_model);
    vx_buffer_swap(vb_car_model);
  }
  else {
    state->enable_viewer = 0;
    state->vw = NULL;
    state->webvx = NULL;
  }

  state->vertical_angle_sincos = malloc(32 * 2 * sizeof(double));
  for(int i = 0; i < 32; ++i) {
    double rad = vertical_angle_degrees[i] * M_PI / 180.0;
    state->vertical_angle_sincos[2 * i + 0] = sin(rad);
    state->vertical_angle_sincos[2 * i + 1] = cos(rad);
  }

  state->cropping_min_points = CROPPING_MIN_POINTS;
  state->cropping_voxel_radius = CROPPING_VOXEL_RADIUS;
  state->interval_range_threshold = INTERVAL_RANGE_THRESHOLD;
  state->interval_circular_levels = INTERVAL_CIRCULAR_LEVELS;
  state->interval_row_start = INTERVAL_ROW_START;
  state->interval_col_start = INTERVAL_COL_START;
  state->normal_min_points = NORMAL_MIN_POINTS;
  state->cluster_point_min_range = CLUSTER_POINT_MIN_RANGE;
  state->cluster_min_dimension = CLUSTER_MIN_DIMENSION;
  state->cluster_neighbouring_radius = CLUSTER_NEIGHBOURING_RADIUS;
  state->cluster_point_distance_threshold = CLUSTER_POINT_DISTANCE_THRESHOLD;
  state->cluster_normal_angle_threshold = CLUSTER_NORMAL_ANGLE_THRESHOLD;
  state->line_max_error = LINE_MAX_ERROR;
  state->line_max_eigenvalues_ratio = LINE_MAX_EIGENVALUES_RATIO;
  state->line_min_length = LINE_MIN_LENGTH;
  state->plane_max_error = PLANE_MAX_ERROR;
  state->plane_max_eigenvalues_ratio = PLANE_MAX_EIGENVALUES_RATIO;
  state->plane_min_points = PLANE_MIN_POINTS;
  state->full_point_cloud = NULL;
  state->point_cloud = NULL;
  state->point_integral_image = NULL;
}

int main(int argc, char **argv) {
  // handle input
  printf("[INFO] usage: velodyne32-features-calculator [-enable_viewer [port]]\n");
  printf("[INFO] \texample --> velodyne32-features-calculator -enable_viewer 3389\n");
  int enable_viewer = 0;
  int port = 3389;
  if(argc > 1 && strcmp(argv[1], "-enable_viewer") == 0) {
      enable_viewer = 1;
      if(argc > 2) {
	port = atoi(argv[2]);
      }
  }

  // init state
  state_t *state = calloc(1, sizeof(state_t));
  init_state(state, enable_viewer, port);

  // subscribe to velodyne data channel
  image_t_subscribe(state->lcm, "VELODYNE_DEPTH_IMAGE", &velodyne_depth_image_callback, state);
  
  // run the loop
  while(1) {
    lcm_handle(state->lcm);
  }

  free(state->vertical_angle_sincos);
  lcm_destroy(state->lcm);
  point_cloud_destroy(state->full_point_cloud);
  point_cloud_destroy(state->point_cloud);
  integral_image_destroy(state->point_integral_image);
  free(state);

  return 0;
}
