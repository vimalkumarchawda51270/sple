/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#ifndef POINT_ACCUMULATOR_H
#define POINT_ACCUMULATOR_H

#include <common/matd.h>
#include <common/mati.h>

struct point_accumulator {
  matd_t *sum;
  matd_t *squared_sum;
};
typedef struct point_accumulator point_accumulator_t;

point_accumulator_t* point_accumulator_create();

void point_accumulator_destroy(point_accumulator_t *to_destroy);

void point_accumulator_print(point_accumulator_t *to_print);

void point_accumulator_clear(point_accumulator_t *pa);

void point_accumulator_add_inplace(point_accumulator_t *pa_left, 
				   point_accumulator_t *pa_right);

void point_accumulator_sub_inplace(point_accumulator_t *pa_left, 
				   point_accumulator_t *pa_right);

void point_accumulator_add_point_inplace(point_accumulator_t *pa, matd_t *point);

void point_accumulator_add_normal_inplace(point_accumulator_t *pa, matd_t *normal);

inline int point_accumulator_n(point_accumulator_t *pa) { 
  return (int)matd_get(pa->sum, pa->sum->nrows - 1, 0);
}

matd_t* point_accumulator_mean(point_accumulator_t *pa);

matd_t* point_accumulator_covariance(point_accumulator_t *pa);

#endif
