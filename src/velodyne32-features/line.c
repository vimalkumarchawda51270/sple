/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

#include "line.h"

line_t* line_create_zeros() {
  line_t *l = (line_t*)calloc(1, sizeof(line_t));
  l->origin = matd_create(4, 1);
  matd_put(l->origin, l->origin->nrows - 1, 0, 1.0f);
  l->direction = matd_create(4, 1);
  return l;
}

void line_destroy(line_t *to_destroy) {
  if(to_destroy == NULL) {
    return;
  }
  matd_destroy(to_destroy->origin);
  matd_destroy(to_destroy->direction);
  memset(to_destroy, 0, sizeof(line_t));
  free(to_destroy);
}

void line_print(line_t *to_print) {
  printf("Origin = ");
  matd_print_transpose(to_print->origin, "%f ");
  printf("\n");
  printf("Direction = ");
  matd_print_transpose(to_print->direction, "%f ");
  printf("\n\n");
}

void line_set_zeros(line_t *l) {
  for(int i = 0; i < l->origin->nrows; ++i) {
    matd_put(l->origin, i, 0, 0.0);
    matd_put(l->direction, i, 0, 0.0);
  }
  matd_put(l->origin, l->origin->nrows - 1, 0, 1.0f);  
}

void line_set_origin(line_t *l, double *origin) {
  for(int i = 0; i < l->origin->nrows; ++i) {
    matd_put(l->origin, i, 0, origin[i]);
  }
}

void line_set_direction(line_t *l, double *direction) {
  for(int i = 0; i < l->direction->nrows; ++i) {
    matd_put(l->direction, i, 0, direction[i]);
  }
}

void line_draw(line_t *l, vx_world_t *vw, vx_buffer_t *buffer, float line_length, int line_width) {
  zarray_t *lines_vertices = zarray_create(sizeof(float[3]));
  float t = line_length;
  float origin[3] = { (float)l->origin->data[0], 
		      (float)l->origin->data[1], 
		      (float)l->origin->data[2] };
  float other_positve[3] = { (float)l->origin->data[0] + t * (float)l->direction->data[0], 
			     (float)l->origin->data[1] + t * (float)l->direction->data[1], 
			     (float)l->origin->data[2] + t * (float)l->direction->data[2] };
  float other_negative[3] = { (float)l->origin->data[0] - t * (float)l->direction->data[0], 
			      (float)l->origin->data[1] - t * (float)l->direction->data[1], 
			      (float)l->origin->data[2] - t * (float)l->direction->data[2] };  
  zarray_add(lines_vertices, origin);
  zarray_add(lines_vertices, other_positve);
  zarray_add(lines_vertices, origin);
  zarray_add(lines_vertices, other_negative);

  float line_color[4] = { 1.0f, 0.6f, 0.2f, 1.0f };
  vx_buffer_add_back(buffer,
		     vxo_lines(vx_resource_make_attr_f32_copy((float*)lines_vertices->data,
							      3 * zarray_size(lines_vertices),
							      3),
			       line_color, line_width),
		     NULL);

  zarray_destroy(lines_vertices);
}

double line_point_distance_compute(line_t *l, matd_t *p) {
  // select non-homogeneous part of the vectors  
  matd_t *po = matd_select(l->origin, 0, 2, 0, 0);
  matd_t *dir = matd_select(l->direction, 0, 2, 0, 0);
  matd_t *point = matd_select(p, 0, 2, 0, 0);
  
  matd_subtract_inplace(po, point);
  matd_t *cross = matd_crossproduct(po, dir);
  double distance =  matd_vec_mag(cross) / matd_vec_mag(dir);
  
  matd_destroy(po);
  matd_destroy(dir);
  matd_destroy(point);
  matd_destroy(cross);

  return distance;
}

line_t* line_through2points_compute(matd_t *p1, matd_t *p2) {
  line_t *line = line_create_zeros();
  matd_t *direction = matd_subtract(p2, p1);
  line_set_origin(line, p1->data);
  line_set_direction(line, direction->data);
  matd_destroy(direction);
  return line;
}

int line_fitting_compute(line_t** line, point_cloud_t *point_cloud, zarray_t *data,
			 double eigenvalues_threshold, double min_length) {
int good_fitting_line = 0;
  int data_size = zarray_size(data);
  point_accumulator_t *pa = point_accumulator_create();  
  double max[3] = { -10000.0, -10000.0, -10000.0 };
  double min[3] = { 10000.0, 10000.0, 10000.0 };
  for(int i = 0; i < data_size; ++i) {
    int *index;
    zarray_get_volatile(data, i, &index);
    rich_point_t *rp;
    zarray_get_volatile(point_cloud->measurements, *index, &rp);
    point_accumulator_add_point_inplace(pa, rp->point);
    max[0] = rp->point->data[0] > max[0] ? rp->point->data[0] : max[0];
    max[1] = rp->point->data[1] > max[1] ? rp->point->data[1] : max[1];
    max[2] = rp->point->data[2] > max[2] ? rp->point->data[2] : max[2];
    min[0] = rp->point->data[0] < min[0] ? rp->point->data[0] : min[0];
    min[1] = rp->point->data[1] < min[1] ? rp->point->data[1] : min[1];
    min[2] = rp->point->data[2] < min[2] ? rp->point->data[2] : min[2];    
  }
  double distance[3] = { max[0] - min[0], max[1] - min[1], max[2] - min[2] };
  double d = sqrt(distance[0]*distance[0] + distance[1]*distance[1] + distance[2]*distance[2]);

  *line = line_create_zeros();
  matd_t *origin = point_accumulator_mean(pa);
  matd_t *covariance = point_accumulator_covariance(pa);
  matd_svd_t svd = matd_svd_flags(covariance, 1);
  double ratio = (matd_get(svd.S, 2, 2) + matd_get(svd.S, 1, 1) / 
		  (matd_get(svd.S, 2, 2) + matd_get(svd.S, 1, 1) + matd_get(svd.S, 0, 0)));
  double direction[4] = { matd_get(svd.U, 0, 0), matd_get(svd.U, 1, 0), matd_get(svd.U, 2, 0),
  			  0.0f };
  if(ratio > eigenvalues_threshold || d < min_length) {
    line_destroy(*line);
    *line = NULL;
  }
  else {
    line_set_origin(*line, origin->data);
    line_set_direction(*line, direction);
    good_fitting_line = 1;
  }

  matd_destroy(svd.U);
  matd_destroy(svd.S);
  matd_destroy(svd.V);
  matd_destroy(covariance);
  matd_destroy(origin);
  point_accumulator_destroy(pa);

  return good_fitting_line;
}

double line_fitting_error_compute(line_t *l, point_cloud_t *point_cloud, zarray_t *data) {
  double error = 0.0;
  int data_size = zarray_size(data);  
  for(int i = 0; i < data_size; ++i) {
    int *index;
    zarray_get_volatile(data, i, &index);
    rich_point_t *rp;
    zarray_get_volatile(point_cloud->measurements, *index, &rp);
    error += line_point_distance_compute(l, rp->point);
  }
  return error / data_size;
}
