/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <unistd.h>

#include <vx/vx.h>
#include <vx/webvx.h>

#include "velodyne32-features/line.h"

#define ROWS 10
#define COLS 20

struct state {
  vx_world_t *vw;
  webvx_t *webvx;
};
typedef struct state state_t;

void on_create_canvas(vx_canvas_t *vc, const char *name, void *impl) {
  state_t *state = (state_t*)impl;

  printf("[INFO] on create canvas\n");
  vx_layer_t *vl = vx_canvas_get_layer(vc, "default");

  vx_layer_set_world(vl, state->vw);
  float background_color[4] = { 0.95, 0.95, 0.96, 1.0 };
  vx_layer_set_background_rgba(vl, background_color, 0);
}

void on_destroy_canvas(vx_canvas_t *vc, void *impl) {
  state_t *state = (state_t*)impl;
  printf("[INFO] on destroy canvas\n");
}

int main(int argc, char **argv) {
  point_cloud_t * point_cloud = point_cloud_create(sizeof(rich_point_t));
  point_cloud_pre_allocate_structures(point_cloud, COLS, ROWS);
  zarray_t *data = zarray_create(sizeof(int));
  
  int point_cloud_size = zarray_size(point_cloud->measurements);
  for(int index = 0; index < point_cloud_size; ++index) {
    double x = 2.0 * (double)rand() / (double)RAND_MAX;
    double y = 0.1 * (double)rand() / (double)RAND_MAX;
    double z = 0.1 * (double)rand() / (double)RAND_MAX;
    double point[4] = { x, y, z, 1.0 };      
    rich_point_t *rich_point; 
    zarray_get_volatile(point_cloud->measurements, index, &rich_point);        
    rich_point_set_point(rich_point, point);
    int i = index;
    zarray_add(data, &i);
  }

  line_t *fitting_line;
  line_fitting_compute(&fitting_line, point_cloud, data, 1.0, 0.0);
  line_print(fitting_line);
  
  state_t *state = calloc(1, sizeof(state_t));
  state->vw = vx_world_create();
  state->webvx = webvx_create_server(10000, NULL, "index.html");
  printf("[INFO] viewer enabled on port 10000\n");
  webvx_define_canvas(state->webvx, "line_fitting_test_viewer_canvas",
		      on_create_canvas, on_destroy_canvas, state);
  float color[4] = { 0.0, 0.3, 1.0, 1.0 };
  point_cloud_draw(point_cloud, state->vw, "points", color, 3, 0.0, 0.0);
  vx_buffer_t *vb_line = vx_world_get_buffer(state->vw, "line");
  line_draw(fitting_line, state->vw, vb_line, 1.0, 5);
  vx_buffer_swap(vb_line);  
  while(1) {
    usleep(10000);
  }

  free(state);
  line_destroy(fitting_line);
  zarray_destroy(data);
  point_cloud_destroy(point_cloud);

  return 0;
}
