/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#ifndef LINE_H
#define LINE_H

#include <common/matd.h>
#include <common/zarray.h>

#include "point_cloud.h"

struct line {
  matd_t *origin;
  matd_t *direction;
};
typedef struct line line_t;

line_t* line_create_zeros();

void line_destroy(line_t *to_destroy);

void line_print(line_t *to_print);

void line_set_zeros(line_t *l);

void line_set_origin(line_t *l, double *origin);

void line_set_direction(line_t *l, double *direction);

void line_draw(line_t *l, vx_world_t *vw, vx_buffer_t *buffer, float line_length, int line_width);

double line_point_distance_compute(line_t *l, matd_t *p);

line_t* line_through2points_compute(matd_t *p1, matd_t *p2);

int line_fitting_compute(line_t** line, point_cloud_t *point_cloud, zarray_t *data,
			 double eigenvalues_threshold, double min_length);

double line_fitting_error_compute(line_t *l, point_cloud_t *point_cloud, zarray_t *data);

#endif
