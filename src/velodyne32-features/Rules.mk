# List all objects here, or use
LIB_$(D) = $(LIB_PATH)/libvelodyne32features.a
LIBVELODYNE32FEATURES	:= $(LIB_PATH)/libvelodyne32features.a

D	:= $(D)/test
include		$(D)/Rules.mk
D       := $(realpath $(dir $(D)))

LIB_SRC_$(D)	:= $(D)/point_cloud.c $(D)/interval_image.c $(D)/integral_image.c \
	$(D)/point_accumulator.c $(D)/clusterizer.c $(D)/line.c $(D)/plane.c
LIB_OBJS_$(D)	:= $(LIB_SRC_$(D):%.c=%.o)

OBJS_$(D)	:= $(D)/velodyne32_features_image_generator.o \
	$(D)/velodyne32_features_calculator.o

# List all targets, it's important for building and cleaning
TGTS_$(D)	:= $(BIN_PATH)/velodyne32-features-image-generator \
	$(BIN_PATH)/velodyne32-features-calculator

# List CFLAGS used when .o are compiled
$(LIB_OBJS_$(D)): 	CFLAGS_TGT := $(CFLAGS_APRIL)
$(OBJS_$(D)):   	CFLAGS_TGT := $(CFLAGS_APRIL) 

# List LDFlags and libs youn depend on here
$(TGTS_$(D)):   	LDFLAGS_TGT := $(LDFLAGS_LCM) $(LIBVX) $(LIBEK) $(LIBHTTPD) $(LIBCOMMON) $(LIBLCMTYPES)
$(TGTS_$(D)):   	$(LIBVELODYNE32FEATURES) $(LIBUTILS)

$(LIB_$(D)): $(LIB_OBJS_$(D))
	@echo "$@"
	$(AR)

$(BIN_PATH)/velodyne32-features-image-generator: $(D)/velodyne32_features_image_generator.o 
	$(LINK)

$(BIN_PATH)/velodyne32-features-calculator: $(D)/velodyne32_features_calculator.o
	$(LINK)

# If it doesn't make it into TGTS, it won't build
TGTS		:= $(TGTS) $(LIB_$(D)) $(TGTS_$(D))
CLEAN		:= $(CLEAN) $(LIB_$(D)) $(LIB_OBJS_$(D)) $(TGTS_$(D)) $(OBJS_$(D))
