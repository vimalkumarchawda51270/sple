/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

#include <common/math_util.h>

#include "interval_image.h"

void interval_print(interval_t *to_print) {
  printf("Interval = [%d, %d], [%d, %d]\n\n", 
	 to_print->left_col, to_print->up_row,
	 to_print->right_col, to_print->bottom_row);
}

void interval_clear(interval_t *i) {
  i->left_col = -1;
  i->up_row = -1;
  i->right_col = -1;
  i->bottom_row = -1;
}

interval_image_t* interval_image_create(int width, int height) {
  interval_image_t *im = (interval_image_t*)calloc(1, sizeof(interval_image_t));

  im->width = width;
  im->height = height;

  im->buf = calloc(im->height * im->width, sizeof(interval_t));

  return im;
}

void interval_image_destroy(interval_image_t *im) {
  free(im->buf);
  free(im);
}

void interval_image_clear(interval_image_t *im) {
#pragma omp parallel for
  for(int r = 0; r < im->height; ++r) {
    for(int c = 0; c < im->width; ++c) {
      interval_clear(im->buf + r * im->width + c);
    }
  }
}

int _check_subcol(int *up_row, int *bottom_row,
		  mati_t *indices, image_t *depth_image,
		  int central_row, int col, int start_row, int end_row, 
		  uint16_t reference_range, uint16_t range_threshold) {
  *up_row = -1;
  *bottom_row = -1;
  int found_valid_element = 0;
  int *index = indices->data;
  uint16_t *depth_data = (uint16_t*)depth_image->data;
 
  if(index[central_row * indices->ncols + col] >= 0) {
    if(abs(depth_data[central_row * depth_image->width + col] - reference_range) < range_threshold) {
      *up_row = *up_row + 1;
      *bottom_row = *bottom_row + 1;
      found_valid_element = 1;;
    }
    else {
      return found_valid_element;
    } 
  }
  else {
    *up_row = *up_row + 1;
    *bottom_row = *bottom_row + 1;
  }
  
  for(int r = central_row - 1; r >= start_row; --r) {
    if(index[r * indices->ncols + col] < 0) {
      *up_row = *up_row + 1;
    }
    else if(abs(depth_data[r * depth_image->width + col] - reference_range) < range_threshold) {
      *up_row = *up_row + 1;
      found_valid_element = 1;
    } 
    else {
      break;
    }
  }

  for(int r = central_row + 1; r <= end_row; ++r) {
    if(index[r * indices->ncols + col] < 0) {
      *bottom_row = *bottom_row + 1;
    }
    else if(abs(depth_data[r * depth_image->width + col] - reference_range) < range_threshold) {
      *bottom_row = *bottom_row + 1;
      found_valid_element = 1;
    } 
    else {
      break;
    }
  }

  return found_valid_element;
}

int _check_subrow(int *left_col, int *right_col,
		  mati_t *indices, image_t *depth_image,
		  int row, int central_col, int start_col, int end_col, 
		  uint16_t reference_range, uint16_t range_threshold) {
  *left_col = -1;
  *right_col = -1;
  int found_valid_element = 0;
  int *index = indices->data + row * indices->ncols;
  uint16_t *depth_data = (uint16_t*)depth_image->data + row * depth_image->width;
  
  if(index[central_col] >= 0) {
    if(abs(depth_data[central_col] - reference_range) < range_threshold) {
      *left_col = *left_col + 1;
      *right_col = *right_col + 1;
      found_valid_element = 1;
    }
    else {
      return found_valid_element;
    } 
  }
  else {
    *left_col = *left_col + 1;
    *right_col = *right_col + 1;
  }

  for(int c = central_col - 1; c >= start_col; --c) {
    if(index[c] < 0) {
      *left_col = *left_col + 1;
    }
    else if(abs(depth_data[c] - reference_range) < range_threshold) {
      *left_col = *left_col + 1;
      found_valid_element = 1;
    } 
    else {
      break;
    }
  }

  for(int c = central_col + 1; c <= end_col; ++c) {
    if(index[c] < 0) {
      *right_col = *right_col + 1;
    }
    else if(abs(depth_data[c] - reference_range) < range_threshold) {
      *right_col = *right_col + 1;
      found_valid_element = 1;
    }
    else {
      break;
    }
  }

  return found_valid_element;
}

interval_image_t* interval_image_compute(mati_t *indices, image_t *depth_image,
					 uint16_t range_threshold, int num_levels,
					 int rows_starting_radius, int cols_starting_radius) {
  assert(depth_image->width > 0 && depth_image->height > 0);
  assert(indices->ncols == depth_image->width && indices->nrows == depth_image->height);
  
  interval_image_t* interval_image = interval_image_create(depth_image->width, depth_image->height);

#pragma omp parallel for
  for(int r = 0; r < interval_image->height; ++r) {
    int *index = indices->data + r * indices->ncols;
    uint16_t *depth_data = (uint16_t*)depth_image->data + r * depth_image->width;
    interval_t *interval = interval_image->buf + r * interval_image->width;
    for(int c = 0; c < interval_image->width; ++c, ++index, ++depth_data, ++interval) {
      if(*index < 0) {
	interval_clear(interval);
	continue;
      }
      
      int keep_going = 1;
      int rows_crown_radius = rows_starting_radius;
      int cols_crown_radius = cols_starting_radius;
      for(int i = 0; i < num_levels && keep_going > 0; ++i) {
	keep_going = 0; 

	int start_row = r - rows_crown_radius;
	if(start_row < 0) {
	  start_row = 0;
	}
	int start_col = c - cols_crown_radius;
	if(start_col < 0) {
	  start_col = 0;
	}
	int end_row = r + rows_crown_radius;
	if(end_row >= interval_image->height) {
	  end_row = interval_image->height - 1;
	}
	int end_col = c + cols_crown_radius;
	if(end_col >= interval_image->width) {
	  end_col = interval_image->width - 1;
	}
	
	int up_left_col, up_right_col;
	keep_going += _check_subrow(&up_left_col, &up_right_col,
				    indices, depth_image,
				    start_row, c, start_col, end_col,
				    *depth_data, range_threshold);
	
	int bottom_left_col, bottom_right_col;
	keep_going += _check_subrow(&bottom_left_col, &bottom_right_col,
				    indices, depth_image,
				    end_row, c, start_col, end_col,
				    *depth_data, range_threshold);

	int left_up_row, left_bottom_row;
	keep_going += _check_subcol(&left_up_row, &left_bottom_row,
				    indices, depth_image,
				    r, start_col, start_row, end_row,
				    *depth_data, range_threshold);

	int right_up_row, right_bottom_row;
	keep_going += _check_subcol(&right_up_row, &right_bottom_row,
				    indices, depth_image,
				    r, end_col, start_row, end_row,
				    *depth_data, range_threshold);
	
	int left_col = imin(up_left_col, bottom_left_col);
	int up_row = imin(left_up_row, right_up_row);
	int right_col = imin(up_right_col, bottom_right_col);
	int bottom_row = imin(left_bottom_row, right_bottom_row);

	if(i == 0) {
	  interval->left_col = left_col;
	  interval->right_col = right_col;
	  interval->up_row = up_row;
	  interval->bottom_row = bottom_row;	  
	}
	else {
	  interval->left_col = interval->left_col > left_col ? interval->left_col : left_col;
	  interval->right_col = interval->right_col > right_col ? interval->right_col : right_col;
	  interval->up_row = interval->up_row > up_row ? interval->up_row : up_row;
	  interval->bottom_row = interval->bottom_row > bottom_row ? interval->bottom_row : bottom_row;
	}

	rows_crown_radius++;
	cols_crown_radius++;
      }
    }
  }

  return interval_image;    
}
