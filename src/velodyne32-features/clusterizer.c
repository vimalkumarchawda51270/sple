/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include "clusterizer.h"

#include <common/doubles.h>
#include <common/unionfind.h>

#include "line.h"
#include "plane.h"

int zqueue_contains_pixel(zqueue_t *zq, pixel_t *pixel) {
  int queue_size = zqueue_size(zq); 
  for(int i = 0; i < queue_size; ++i) {
    pixel_t *candidate_pixel;
    zqueue_get_volatile(zq, i, &candidate_pixel);    
    if(pixel->row == candidate_pixel->row && 
       pixel->col == candidate_pixel->col 
       && pixel->index == candidate_pixel->index) {
      return 1;
    }
  }
  return 0;
}

void cluster_draw(zarray_t *cluster, point_cloud_t *point_cloud, 
		  vx_buffer_t *buffer, int point_size) {
  int cluster_size = zarray_size(cluster);
  zarray_t *points = zarray_create(sizeof(float[3]));
  zarray_ensure_capacity(points, cluster_size);
  for(int i = 0; i < cluster_size; ++i)      {
    int *index;
    zarray_get_volatile(cluster, i, &index);    
    rich_point_t *rp;
    zarray_get_volatile(point_cloud->measurements, *index, &rp);    
    float p[3] = { matd_get(rp->point, 0, 0), 
		   matd_get(rp->point, 1, 0), 
		   matd_get(rp->point, 2, 0) };
    zarray_add(points, p);
  }

  double r = ((double)rand() / (double)RAND_MAX) - 0.1;
  double g = ((double)rand() / (double)RAND_MAX) - 0.1;
  double b = ((double)rand() / (double)RAND_MAX) - 0.1;
  float point_color[4] = { r, g, b, 1.0 };
  vx_buffer_add_back(buffer,
		     vxo_points(vx_resource_make_attr_f32_copy((float*)points->data,
							       3 * zarray_size(points), 3), 
				point_color, point_size),
		     NULL);
  zarray_destroy(points);
}

zarray_t* cluster_point_compute(point_cloud_t *point_cloud, mati_t *indices,
				int min_cluster_dimension, int neighbouring_radius,
				double point_distance_threshold, double point_min_range) {
  assert(zarray_size(point_cloud->measurements));
  assert(indices->ncols > 0 && indices->nrows > 0);    

  double squared_distance_threshold = point_distance_threshold * point_distance_threshold;
  zarray_t *clusters = zarray_create(sizeof(zarray_t*));
  mati_t *tmp_indices = mati_copy(indices);
  for(int r = 0; r < tmp_indices->nrows; ++r) {
    int32_t *index = tmp_indices->data + r * tmp_indices->ncols;
    for(int c = 0; c < tmp_indices->ncols; ++c, ++index) {
      if(*index < 0) {
	continue;
      }

      rich_point_t *starting_seed;
      zarray_get_volatile(point_cloud->measurements, *index, &starting_seed);
      zqueue_t *pool = zqueue_create(sizeof(pixel_t));
      zarray_t *cluster = zarray_create(sizeof(int));
      pixel_t pixel = { r, c, *index };
      zqueue_push(pool, &pixel);
      while(zqueue_size(pool) > 0) {
      	pixel_t p;
      	zqueue_pop(pool, &p);
      	rich_point_t *candidate;
      	zarray_get_volatile(point_cloud->measurements, p.index, &candidate);
      	
	double range = matd_vec_mag(candidate->point);
	double normal_length = matd_vec_mag(candidate->normal);
	double c = candidate->curvature;
	if(range < point_min_range || !(normal_length > 0.0) || !(c > 0.0)) {
	  tmp_indices->data[p.row * tmp_indices->ncols + p.col] = -1;
	  continue;
	}
      	zarray_add(cluster, &p.index);
      	for(int rr = p.row - neighbouring_radius; rr <= p.row + neighbouring_radius; ++rr) {
      	  for(int cc = p.col - neighbouring_radius; cc <= p.col + neighbouring_radius; ++cc) {
      	    if(rr < 0 || rr >= tmp_indices->nrows ||
      	       cc < 0 || cc >= tmp_indices->ncols) {
      	      continue;
      	    }
      	    if(tmp_indices->data[rr * tmp_indices->ncols + cc] < 0) {
      	      continue;
      	    }

	    int ni = tmp_indices->data[rr * tmp_indices->ncols + cc];
	    rich_point_t *np;
	    zarray_get_volatile(point_cloud->measurements, ni, &np);
	    double squared_distance = doubles_squared_distance(candidate->point->data,
							       np->point->data, 3);
	    if(squared_distance > squared_distance_threshold) {
	      continue;
	    }

      	    pixel_t neighbour = { rr, cc, ni };
	    zqueue_push(pool, &neighbour);
	    tmp_indices->data[rr * tmp_indices->ncols + cc] = -1;
      	  }
      	}
      }
      if(zarray_size(cluster) > min_cluster_dimension) {
	zarray_add(clusters, &cluster);
      }
      else {
	zarray_destroy(cluster);
      }
      
      zqueue_destroy(pool);
    }
  }
  
  mati_destroy(tmp_indices);

  return clusters;
}

zarray_t* cluster_point_and_normal_compute(point_cloud_t *point_cloud, mati_t *indices,
					   int min_cluster_dimension, int neighbouring_radius, 
					   double point_distance_threshold, double point_min_range, 
					   double normal_angle_threshold) {
  assert(zarray_size(point_cloud->measurements));
  assert(indices->ncols > 0 && indices->nrows > 0);    

  double squared_distance_threshold = point_distance_threshold * point_distance_threshold;
  double cos_normal_angle_threshold = cos(normal_angle_threshold);  
  zarray_t *clusters = zarray_create(sizeof(zarray_t*));
  mati_t *tmp_indices = mati_copy(indices);
  for(int r = 0; r < tmp_indices->nrows; ++r) {
    int32_t *index = tmp_indices->data + r * tmp_indices->ncols;
    for(int c = 0; c < tmp_indices->ncols; ++c, ++index) {
      if(*index < 0) {
	continue;
      }

      rich_point_t *starting_seed;
      zarray_get_volatile(point_cloud->measurements, *index, &starting_seed);
      zqueue_t *pool = zqueue_create(sizeof(pixel_t));
      zarray_t *cluster = zarray_create(sizeof(int));
      pixel_t pixel = { r, c, *index };
      zqueue_push(pool, &pixel);
      while(zqueue_size(pool) > 0) {
      	pixel_t p;
      	zqueue_pop(pool, &p);
      	rich_point_t *candidate;
      	zarray_get_volatile(point_cloud->measurements, p.index, &candidate);
      	
	double range = matd_vec_mag(candidate->point);
	double normal_length = matd_vec_mag(candidate->normal);
	double c = candidate->curvature;
	if(range < point_min_range || !(normal_length > 0.0) || !(c > 0.0)) {
	  tmp_indices->data[p.row * tmp_indices->ncols + p.col] = -1;
	  continue;
	}
      	zarray_add(cluster, &p.index);
      	for(int rr = p.row - neighbouring_radius; rr <= p.row + neighbouring_radius; ++rr) {
      	  for(int cc = p.col - neighbouring_radius; cc <= p.col + neighbouring_radius; ++cc) {
      	    if(rr < 0 || rr >= tmp_indices->nrows ||
      	       cc < 0 || cc >= tmp_indices->ncols) {
      	      continue;
      	    }
      	    if(tmp_indices->data[rr * tmp_indices->ncols + cc] < 0) {
      	      continue;
      	    }

	    int ni = tmp_indices->data[rr * tmp_indices->ncols + cc];
	    rich_point_t *np;
	    zarray_get_volatile(point_cloud->measurements, ni, &np);
	    double squared_point_distance = doubles_squared_distance(candidate->point->data,
								     np->point->data, 3);
	    if(squared_point_distance > squared_distance_threshold) {
	      continue;
	    }
	    double normal_dot_product = matd_vec_dot_product(candidate->normal, np->normal);
	    if(normal_dot_product < cos_normal_angle_threshold) {
	      continue;
	    }

      	    pixel_t neighbour = { rr, cc, ni };
	    zqueue_push(pool, &neighbour);
	    tmp_indices->data[rr * tmp_indices->ncols + cc] = -1;
      	  }
      	}
      }
      if(zarray_size(cluster) > min_cluster_dimension) {
	zarray_add(clusters, &cluster);
      }
      else {
	zarray_destroy(cluster);
      }
      
      zqueue_destroy(pool);
    }
  }
  
  mati_destroy(tmp_indices);

  return clusters;
}

void cluster_fitting_lines_planes_compute(zarray_t **lines, zarray_t **planes,
					  zarray_t *clusters, point_cloud_t *point_cloud, 
					  double line_max_error, double plane_max_error, 
					  double line_max_eigenvalues_ratio, 
					  double plane_max_eigenvalues_ratio,
					  double line_min_length,
					  int plane_min_points) {
  *lines = zarray_create(sizeof(line_t*));
  *planes = zarray_create(sizeof(plane_t*));
  for(int i = 0; i < zarray_size(clusters); ++i) {
    zarray_t **cluster;
    zarray_get_volatile(clusters, i, &cluster);
    line_t *line = NULL;
    if(line_fitting_compute(&line, point_cloud, *cluster, 
			    line_max_eigenvalues_ratio, line_min_length)) {
      double error = line_fitting_error_compute(line, point_cloud, *cluster);
      if(error < line_max_error) {
	zarray_add(*lines, &line);
      }
      else {
	line_destroy(line);
	line = NULL;
      }
    }
    plane_t *plane = NULL;
    if(line == NULL && zarray_size(*cluster) > plane_min_points && 
       plane_fitting_compute(&plane, point_cloud, *cluster, plane_max_eigenvalues_ratio)) {
      double error = plane_fitting_error_compute(plane, point_cloud, *cluster);
      if(error < plane_max_error) {
	zarray_add(*planes, &plane);
      }
      else {
	plane_destroy(plane);
	plane = NULL;
      }
    }
  }
}
