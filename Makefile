export ROOT_PATH      = $(CURDIR)
export SRC_PATH       = $(ROOT_PATH)/src
export BIN_PATH       = $(ROOT_PATH)/bin
export LIB_PATH       = $(ROOT_PATH)/lib
export TEST_PATH      = $(BIN_PATH)/test
export BUILD_COMMON   = $(ROOT_PATH)/Build.common
export CONFIG_DIR     = $(ROOT_PATH)/config
export APRIL_PATH     = $(ROOT_PATH)/april2
export APRIL_LIB_PATH = $(APRIL_PATH)/lib
export LCM_PATH       = $(ROOT_PATH)/src/lcmtypes

.SUFFIXES:	.c .o


### Build flags for all targets
#
CFLAGS_STD  := 	-std=gnu99 -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE \
		-D_REENTRANT -Wall -Wno-unused-parameter -Wno-unused-variable \
		-Wno-format-zero-length -pthread -fPIC -Wredundant-decls -Werror \
		-I src/ -I april2/src/ -O3 -msse4.2 -fopenmp # -v
LDFLAGS_STD   := 
LINK_LIBS_STD := -lpthread -lz -lm -lgomp

export CFLAGS_LCM := `pkg-config --cflags lcm`
export LDFLAGS_LCM := `pkg-config --libs lcm`
export DEPS_LCM :=

export CFLAGS_GTK := `pkg-config --cflags gtk+-2.0`
export LDFLAGS_GTK := `pkg-config --libs gtk+-2.0`
export DEPS_GTK :=

MAKE := $(MAKE) --no-print-directory


### Build tools
#
CC              = gcc
AR              = @echo "  [$(notdir $(@))]" && ar rc $@ $^
COMP            = @echo "  $(notdir $(@))" && $(CC) $(CFLAGS_STD) $(CFLAGS_TGT) -o $@ -c $<
LINK            = @echo "  [$(notdir $(@))]" && $(CC) $(LDFLAGS_STD) $(LDFLAGS_TGT) -o $@ $^ $(LDFLAGS_TGT) $(LINK_LIBS_STD) $(LINK_LIBS_TGT)
COMPLINK        = @echo "  [$(notdir $(@))]" && $(CC) $(CFLAGS_STD) $(CFLAGS_TGT) $(LDFLAGS_STD) -o $@ $^ $(LINK_LIBS_TGT) $(LINK_LIBS_STD) $(LDFLAGS_TGT)

all:		april2 | targets

D	:= 	$(shell pwd)
include		Rules.mk

.PHONY:		april2
april2:
	$(MAKE) -C $(ROOT_PATH)/april2

.PHONY:		targets
targets:	$(TGTS)

.PHONY:		clean
clean:
	$(MAKE) -C $(ROOT_PATH)/april2 clean
	@rm -f $(CLEAN)



%.o: %.c %.h
	@echo "$@"
	$(COMP)

%.o: %.c
	@echo "$@"
	$(COMP)

%: %.c
	@echo "$@"
	$(COMPLINK)
