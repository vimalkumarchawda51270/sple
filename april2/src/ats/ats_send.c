/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/

#include "ats.h"
#include "common/getopt.h"
#include "lcmtypes/lcmint64_t.h"

#include <pthread.h>
int32_t REMOTE_IP = 0;
int32_t RECV_PORT = 0;

void * sender(void * n){

    // create UDP socket
    int send_socket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (send_socket < 0) {
        perror("socket");
        exit(1);
    }

    int v = 1;
    if (setsockopt(send_socket, SOL_SOCKET, SO_REUSEADDR, &v, sizeof(v))) {
        perror("setsockopt");
        exit(1);
    }

    int recv_sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (recv_sock < 0) {
        printf("Bad recv sock init\n");
        exit(1);
    }

    struct sockaddr_in remote_addr;

    memset(&remote_addr, 0, sizeof(struct sockaddr_in));
    remote_addr.sin_family = AF_INET;
    remote_addr.sin_port = htons(PING_PORT);
    remote_addr.sin_addr.s_addr = REMOTE_IP;
    int32_t p_remote = ntohl(REMOTE_IP);
    printf("Sending to: %d.%d.%d.%d\n",
           (p_remote>>24) & 0xff,
           (p_remote>>16) & 0xff,
           (p_remote>>8) & 0xff,
           (p_remote>>0) & 0xff);

    uint8_t tx[ATS_PACKET_SIZE];
    tx[0] = (ATS_PING_MAGIC >> 24) & 0xff;
    tx[1] = (ATS_PING_MAGIC >> 16) & 0xff;
    tx[2] = (ATS_PING_MAGIC >>  8) & 0xff;
    tx[3] = (ATS_PING_MAGIC >>  0) & 0xff;

    tx[4] = (RECV_PORT >>  24) & 0xff;
    tx[5] = (RECV_PORT >>  16) & 0xff;
    tx[6] = (RECV_PORT >>  8) & 0xff;
    tx[7] = (RECV_PORT >>  0) & 0xff;

    while(1) {
        usleep(500*1000);

        int64_t host_time = utime_now();
        tx[8] = (host_time >> 56) & 0xff;
        tx[9] = (host_time >> 48) & 0xff;
        tx[10] = (host_time >> 40) & 0xff;
        tx[11] = (host_time >> 32) & 0xff;
        tx[12] = (host_time >> 24) & 0xff;
        tx[13] = (host_time >> 16) & 0xff;
        tx[14] = (host_time >>  8) & 0xff;
        tx[15] = (host_time >>  0) & 0xff;

        ssize_t txlen = sendto(send_socket, tx, ATS_PACKET_SIZE, 0, (struct sockaddr*) &remote_addr, sizeof(remote_addr));
        if(txlen != 24) {
            printf("Bad sendlen %zd\n", txlen);
            continue;
        }
    }
}

int main(int argc, char ** argv)
{
    setlinebuf(stdout);

    getopt_t *gopt = getopt_create();
    getopt_add_bool(gopt, 'h', "help", 0, "Show usage");
    getopt_add_string(gopt, 'a', "address", "127.0.0.1", "ipv4 address to ats with");
    getopt_add_string(gopt, 'c', "channel", "ATS", "lcm channel to publish on");
    getopt_add_int(gopt, 'p', "port", "7900", "Port to recieve responses on");
    getopt_add_string(gopt, '\0', "lcm-url", "udpm://239.255.76.67:7667", "LCM Network URL");

    if (!getopt_parse(gopt, argc, argv, 1) || getopt_get_bool(gopt, "help")) {
        printf("Usage: %s \n", argv[0]);
        getopt_do_usage(gopt);
        return 1;
    }

    RECV_PORT = getopt_get_int(gopt, "port");
    printf("Looking for pongs on port: %d\n", RECV_PORT);

    if (inet_pton(AF_INET, getopt_get_string(gopt,"address"), &REMOTE_IP) != 1) {
        printf("inet_pton Failed\n");
        return -1;
    }

    pthread_t sender_t;
    pthread_create(&sender_t, NULL, sender, NULL);

    int recv_sock = udp_socket_listen(RECV_PORT);
    if (recv_sock < 0) {
        printf("UDP Socket Listen\n");
        exit(1);
    }

    lcm_t * lcm = lcm_create(getopt_get_string(gopt, "lcm-url"));
    if(!lcm) {
        printf("LCM failed address: %s\n", getopt_get_string(gopt, "lcm-url"));
        exit(1);
    }

    const char * channel = getopt_get_string(gopt, "channel");

    while (1) {
        uint8_t rx[ATS_PACKET_SIZE];
        uint32_t rxpos = 0;

        struct sockaddr_in src_addr;
        socklen_t src_addr_len = sizeof(src_addr);

        ssize_t rxlen = recvfrom(recv_sock, rx, ATS_PACKET_SIZE, 0,
                                 (struct sockaddr*) &src_addr, &src_addr_len);
        int64_t new_time = utime_now();

        if(rxlen != ATS_PACKET_SIZE) {
            printf("Bad length %zd\n", rxlen);
            continue;
        }

        if (rx[0] != ((ATS_PONG_MAGIC >> 24) & 0xff) ||
            rx[1] != ((ATS_PONG_MAGIC >> 16) & 0xff) ||
            rx[2] != ((ATS_PONG_MAGIC >>  8) & 0xff) ||
            rx[3] != ((ATS_PONG_MAGIC >>  0) & 0xff) ) {
            printf("Bad magic\n");
            continue;
        }

        int64_t diff1 = 0;
        for (int i = 0; i < 8; i++) {
            diff1 = (diff1 << 8) + rx[i+4];
        }

        int64_t remote_time = 0;
        for (int i = 0; i < 8; i++) {
            remote_time = (remote_time << 8) + rx[i+12];
        }
        int64_t diff2 = new_time - remote_time;


        int64_t off = (diff2-diff1)/2;
        //printf("%"PRId64" %"PRId64" offset %"PRId64"\n", diff1, diff2, off);

        lcmint64_t msg;
        msg.utime = new_time;
        msg.data = off;

        lcmint64_t_publish(lcm, channel, &msg);
    }
}
